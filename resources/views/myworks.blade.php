<!--This view is replaced with myworks.html is public folder.-->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aqeeb</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.com/libraries/bulma">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/myprojects.css') }}" media="all" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <div class="flex-center position-ref full-height">

                <div class="top-left links">
                        <a href="{{ route('home') }}">Home</a>
                </div>

                <div class="top-right links">
                    <a href="{{ route('resume') }}">Resume</a>
                </div>

            <div class="content">

                <div class="title m-b-md">
                    Projects & codes
                </div>

                <div class="links">
                    <strong>I would love to get any feedback for the projects below! :)</strong>
                    <br></br>
                    <br></br>
                    <strong>To-Do App with authentication:</strong> <a href="https://gitlab.com/aqeebimtiaz/to_do_app">Gitlab</a><a href="https://aqeeb-basic-tasks-app.herokuapp.com">Heroku Site</a>
                    <div id="root">
                            <modal v-if="showModal" @close="showModal = false">
                                <h4> You will be redirected to your default email client shortly! </h4>
                            </modal>
                            <button @click="showModal = true; location.href='/resume'" @close="showModal = false">Preview</button>
                    </div>
                    <br>
                    <br>
                    <strong>Blog site made during Laravel Training:</strong> <a href="https://gitlab.com/aqeeb01/my-freaking-blog">Gitlab</a><a href="http://aqeeb-cool-blog.herokuapp.com">Heroku Site</a>
                    <br>
                    <br>
                    <a href="https://gitlab.com/aqeebimtiaz"><em>Visit Gitlab for more!</em>
                    <br>
                    <img src="img/Gitlab.png" alt="gitlab" height="30" width="30"></a>
                </div>
            </div>
        </div>

        <script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>
        <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#ppimg').fadeIn(4000);
            });
        </script> --}}
    </body>
</html>
