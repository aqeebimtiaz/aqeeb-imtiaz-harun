<!--This view is replaced with index.html is public folder.-->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aqeeb Imtiaz Harun</title>

        <!-- Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.com/libraries/bulma">

        <!-- Styles-->
        <link href="{{ asset('css/welcome.css') }}" media="all" rel="stylesheet" type="text/css" />



    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div id="content" class="content">
                <div><img src="img/pp.jpg" id="ppimg" alt="Avatar" height="200" width="200"></div>
                <br>
                <strong>Hi! I'm</strong>
                <div class="title m-b-md">
                    Aqeeb Imtiaz Harun
                </div>

                <div class="links">
                    <strong>Fresh graduate in Software Engineering and a Web developer with an obsession to learn & experiment!</strong>
                    <br>
                    <a href="mailto:aqeebimtiaz@gmail.com?subject=I%20Found%20You,%20Aqeeb&body=Let%27s%20talk%20about%20Aqeeb.">Mail me!</a>
                    <a href="{{ route('resume') }}">Resume</a>
                    <a href="{{ route('contact') }}">Contact me</a>
                    <a href="{{ route('projects') }}">Projects</a>
                </div>
            </div>
        </div>
        <footer>Copyright of Aqeeb Imtiaz Harun. Made with <a href="https://laravel.com">Laravel</a></footer>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                //$('#ppimg').fadeIn(4000);
                $("#content").addClass('animated fadeInLeft');
            });
        </script>

    </body>
</html>
