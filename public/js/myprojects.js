// import Vue from 'vue'
//import VueRouter from 'vue-router'

// Vue.use(VueRouter);

// const resume = {
//     template: '<div>Resume</div>'
// }

// const routes = [{
//     path: '/resume',
//     component: resume
// }]

// const router = new VueRouter({
//     routes
// })


Vue.component('modal', {
    template: `
        <div class="modal is-active">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="box">
                    <slot></slot>
                    <!-- Any other Bulma elements you want -->
                </div>
            </div>
            <button class="modal-close is-large" aria-label="close" @click="$emit('close')"></button>
        </div>
        `
});

new Vue({
    el: '#coolTasksApp',

    data: {
        showImageModal: false
    }
})

Vue.component('modal-image', {
    template: `
        <div class="modal is-active">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="box">
                    <slot></slot>
                    <!-- Any other Bulma elements you want -->
                </div>
            </div>
            <button class="modal-close is-large" aria-label="close" @click="$emit('close')"></button>
        </div>
        `
});

new Vue({
    el: '#root',
    el: '#basicTasksApp',
    //router: router,

    data: {
        showModal: false
    }
});

// import MarqueeText from 'vue-marquee-text-component'

// Vue.component('marquee-text', MarqueeText);
