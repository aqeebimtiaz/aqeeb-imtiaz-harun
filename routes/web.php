<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return \File::get(public_path() . '/index.html');
})->name('home');

Route::get('/contact', function () {
    return view('contactMe');
})->name('contact');

Route::get('/projects', function () {
    //return view('myworks');
    return \File::get(public_path() . '/myworks.html');
})->name('projects');

Route::get('/resume', 'FileController@getDownload')->name('resume');